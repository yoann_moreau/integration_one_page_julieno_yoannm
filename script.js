var bm = document.getElementById('burger-menu');
var menu = document.getElementById('menu');
var menuLinks = menu.getElementsByTagName('a');

bm.addEventListener('click', function(){
    menu.classList.toggle('active');
    bm.classList.toggle('active');
});

for (var i = 0; i < menuLinks.length; i++) {
    menuLinks[i].addEventListener('click', function() {
        menu.classList.remove('active');
        bm.classList.remove('active');
    });
}

